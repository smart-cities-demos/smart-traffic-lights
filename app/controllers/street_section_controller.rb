class StreetSectionController < ApplicationController
    def list 
        @street_sections = StreetSection.all
        render :json => @street_sections
    end

    def show
        @street_section = StreetSection.find(params[:id])
        render :json => @street_section
    end
end
