class SensorController < ApplicationController
    def show
        #simulated!
        #----------
        id = params[:id].to_i
        #flux  = Math.cos((Time.now.sec + id)/10.0).abs*10 #0-10 vechiles/sec
        #speed = Math.sin((Time.now.sec + id)/10.0).abs*60 #0-60 km/hour
        #----------
        hash  = calculate_speed_flux(params[:id].to_i)
        @sensor = JSON.generate(hash)
        render :json => @sensor
    end
    
    def list 
        vector = {}
        id = 1

        for j in Sensor.all
            vector[id] = calculate_speed_flux(id);
            id = id+1
        end
        @sensor = JSON.generate(vector)
        render :json => @sensor
    end

    #calcula velocidade com base no estado do semaforo imediatamente a frente do sensor
    def calculate_speed_flux(id)
        openClose = true
        r = Random.rand(0...10)
        if r <5
            openClose = true
        else
            openClose = false
        end

        speed = 0
        flux = 0
        previous_flux = 0
        previous_speed = 0

        state = TrafficLight.where(sensor_id: id)
        count = 0
        for k in state
            #stateAux = k
            count = count + 1;
            openClose = k.init_state
        end
        sensorAux = Sensor.where(id: id)
        
        for k in sensorAux
            previous_speed = k.speed
            previous_flux = k.density
        end

        if openClose==false
            #OPEN traffic light
            speed = previous_speed + Random.rand(2...4)
            #speed = speed - Random.rand(0...4)
          
            flux = previous_flux- Random.rand(1...3)
            #flux = flux + Random.rand(0...6)
        
        else  
            #CLOSED traffic light
            speed = previous_speed - Random.rand(2...4)
            flux = previous_flux + Random.rand(1...3)
        end
        speed = 60 if speed > 60
        speed = 0 if speed < 0
        
        flux = 10 if flux > 10
        flux = 0 if flux < 0
        
        hash  = {:id => id, :speed => speed, :flux => flux}
        
        #Update at Database
        for k in sensorAux
            k.speed = speed
            k.density = flux
            k.save
        end

        return hash
    end
end
