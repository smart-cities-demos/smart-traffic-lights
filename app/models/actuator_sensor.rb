class ActuatorSensor < ActiveRecord::Base
    belongs_to :actuator
    belongs_to :sensor
end
