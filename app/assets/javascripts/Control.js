/*=======================================
 *
 * Control class
 *
 *---------------------------------------
 * This object works as a handler for
 * buttons and checkboxes inside the
 * control panel beside the map.
 * It provides, therefore, control elements
 * so the user can enable highlights and
 * icons over the map, change its color
 * and so on.
 ========================================*/

var Control = function(map) {
    this.map = map;
    this.lightsCheckbox = false;
    this.dataCheckbox = false;

    $('#chk_lights').on('click', this.lightsClicked());
    $('#chk_data').on('click', this.dataClicked());
    $('#td_speed').on('click', this.infoSpeedClicked());
    $('#td_vehicles').on('click', this.infoVolumeClicked());
}

Control.prototype = {

    constructor: Control,

    lightsClicked: function() {
        var control = this;
        return function() {
            control.toggleVisibility($('#chk_lights'), control.map.trafficLights);
            control.lightsCheckbox = !control.lightsCheckbox;
        }
    },

    dataClicked: function() {
        var control = this;
        return function() {
            control.toggleVisibility($('#chk_data'), control.map.streetSections);
            control.dataCheckbox = !control.dataCheckbox;
        }
    },

    infoSpeedClicked: function() {
        var control = this;
        return function() {
            var imageUrl = 'assets/trafficspeed.png';
            $('#infobox').prop('src', imageUrl);
            sections = control.map.streetSections;
            for (i in sections)
                sections[i].changeToSpeedMode();
        }
    },

    infoVolumeClicked: function() {
        var control = this;
        return function() {
            var imageUrl = 'assets/trafficvolume.png';
            $('#infobox').prop('src', imageUrl);
            sections = control.map.streetSections;
            for (i in sections)
                sections[i].changeToVolumeMode();
        }
    },

    toggleVisibility: function(element, streetSections) {
        ($(element).prop('checked') == true) ?
            this.setVisibilityInAll(streetSections, true):
            this.setVisibilityInAll(streetSections, false);
    },

    setVisibilityInAll: function(streetSections, boolean) {
        for (i in streetSections)
            streetSections[i].setVisibility(boolean);
    }
}
