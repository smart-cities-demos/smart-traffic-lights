/*=======================================
 *
 * StreetMap class
 *
 *---------------------------------------
 * This object represents a map proxy for
 * the actual Google Map object (GMO). It allows
 * other objects to communicate with a GMO
 * only through a set of very limited actions.
 * It also provides specific functionalities
 * related to the app, like handling a list
 * of drawn traffic lights for instance. 
 ========================================*/

var StreetMap = function(location, element, zoom) {
    this.map = new google.maps.Map(element, {
        center: location,
        zoom: zoom
    });
    this.trafficLights  = {};
    this.streetSections = [];
    this.actuatorInfo   = {};
    this.sensors = {};
    //this.yellowTrafficLight = [[],[],[],[],[],  [],[],[],[],[]];

    //actuatorTime[x] = list of actuators at time x
    this.actuatorTime   = [[],[],[],[],[], [],[],[],[],[]]; 
    this.location = location;
    this.element  = element;
    this.timeCtrl = 0;
    this.zoom = zoom;
    this.map.addListener('zoom_changed', this.zoomChanged());

    var m = this;
    window.setInterval(function(){ m.getSensorsData(m); }, 4000);
}

StreetMap.prototype = {

    constructor: StreetMap,

    drawTrafficLights: function() {
        var map = this;
        return $.get('/traffic_light').success(function(lights) {
            for (var i in lights) {
                var l = lights[i];
                var position  = {lat: l.latitude, lng: l.longitude}; 
                var color = l.init_state ? "green" : "red";
                var street_ref = l.street;
                var way_to = l.way;
                var id = l.id;
                var light = new TrafficLight(position, map.map.getZoom(), map, color, street_ref, way_to, id);
                map.trafficLights[l.id] = light;
            }
            map.linkActuatorSensor();
        });
        
    },


    drawStreetSections: function() {
        var map = this;
        return $.get("street_section").then(function(sections) {
            for (var i in sections) {
                var s = sections[i];
                var ini_pos  = {lat: s.latitude_start, lng: s.longitude_start}; 
                var end_pos  = {lat: s.latitude_end, lng: s.longitude_end};
                var position = [ini_pos, end_pos];
                var c1 = Math.floor(Math.random()*6);
                var c2 = Math.floor(Math.random()*6);
                var section = new StreetSection(position, speedColor[c1], 
                    volumeColor[c2], map, parseInt(i)+1);
                map.streetSections.push(section);
            }
        });     
    },

    closeInfoWindows: function() {
        for (var i in this.streetSections)
            this.streetSections[i].closeInfoWindow();

        for (var i in this.trafficLights)
            this.trafficLights[i].closeInfoWindow();
    },

    zoomChanged: function() {
        var map = this;
        return function() {
            for (var i in map.trafficLights) {
                var trafficLight = map.trafficLights[i];
                var icon = trafficLight.chooseIcon(map.map.getZoom(), trafficLight.color);
                trafficLight.light.setIcon(icon);
            }
        };
    },
    
    linkActuatorSensor: function(){
        // actuatorInfo = [ [trafficLights], [Sensors] ]
        // trafficLight = [ [paulista], [other] ]
        // paulista/other = [ id, id, id,..., id ]
        // Sensors      = [ id, paulista? ]
        var map = this;
        
        $.get("actuator_sensor", function(data, status)
        {
            if(status == 'success'){
                for (var i in data) 
                {
                    var s = data[i];
                    
                    if(map.actuatorInfo[s.actuator_id] == undefined)
                    {
                        map.actuatorInfo[s.actuator_id] = [[[],[]], []];
                        map.actuatorTime[0].push(s.actuator_id);
                        //map.yellowTrafficLight[9].push(s.actuator_id);
                    }
                    map.actuatorInfo[s.actuator_id][1].push([s.sensor_id, s.direct_influence]);
                    //alert(map.actuatorInfo[s.actuator_id][1][map.actuatorInfo[s.actuator_id][0].length-1][1] + " = " + s.sensor_id);
                }
            }
            map.linkActuatorTrafficLight();
        });
    },
    
    
    linkActuatorTrafficLight: function(){
        // actuatorInfo = [ [trafficLights], [Sensors] ]
        // trafficLight = [ [paulista], [other] ]
        // paulista/other = [ id, id, id,..., id ]
        // Sensors      = [ id, paulista? ]
        var map = this;
        $.get("traffic_light", function(data, status)
        {
            if(status == 'success'){
                for (var i in data) 
                {
                    var s = data[i];
                    if(s.init_state){ //Paulista
                        map.actuatorInfo[s.actuator_id][0][0].push(s.id);
                    }
                    else{
                        map.actuatorInfo[s.actuator_id][0][1].push(s.id);
                    }
                }
            }
        });
    },
    


    getSensorsData: function(map){
        // actuatorInfo = [ [trafficLights], [Sensors] ]
        // trafficLight = [ [paulista], [other] ]
        // paulista/other = [ id, id, id,..., id ]
        // Sensors      = [ id, paulista? ]
        $.get("/sensor").success(function(data){
            for(var i in data){
                var s = data[i];
                map.sensors[s.id] = [s.flux, s.speed];
            }

            //Actuators ready to change their status
            for(var actId in map.actuatorTime[map.timeCtrl]){
                var streetFlux0 = 0; //Paulista
                var streetSpeed0 = 0;//Paulista
                var streetFlux1 = 0; //Other street
                var streetSpeed1 = 0;//Other street

                var id = map.actuatorTime[map.timeCtrl][actId];

                //for each sensor linked in this actuator
                for(var j in map.actuatorInfo[id][1]){
                    var sens = map.actuatorInfo[id][1][j];
                    //At Paulista
                    if(sens[1] == true) {
                        streetFlux0  += map.sensors[sens[0]][0];
                        streetSpeed0 += map.sensors[sens[0]][1];
                    }
                    else{
                        streetFlux1  += map.sensors[sens[0]][0];
                        streetSpeed1 += map.sensors[sens[0]][1];
                    }
                }

                var ratio0 = streetFlux0/(streetSpeed0+1);
                var ratio1 = streetFlux1/(streetSpeed1+1);
                
            
                //Paulista's trafficLight 
                var tl = map.actuatorInfo[id][0][0][0];
                var color0;
                var color1;
                var wait;

                if(map.trafficLights[tl].color.localeCompare("green")==0){
                    color0 = "red";
                    color1 = "green";
                    if(ratio1<ratio0){ wait = 3; }
                    else if(ratio1<10*ratio0){ wait = 4; }
                    else if(ratio1<20*ratio0){ wait = 5; }
                    else if(ratio1<30*ratio0){ wait = 6; }
                    else if(ratio1<45*ratio0){ wait = 7; }
                    else if(ratio1<60*ratio0){ wait = 8; }
                    else { wait = 9; }
                }
                else if(map.trafficLights[tl].color.localeCompare("red")==0){
                    color0 = "green";
                    color1 = "red";

                    if(ratio0<ratio1){ wait = 3; }
                    else if(ratio0<10*ratio1){ wait = 4; }
                    else if(ratio0<20*ratio1){ wait = 5; }
                    else if(ratio0<30*ratio1){ wait = 6; }
                    else if(ratio0<45*ratio1){ wait = 7; }
                    else if(ratio0<60*ratio1){ wait = 8; }
                    else { wait = 9; }
                }
            
                map.actuatorTime[(map.timeCtrl + wait)%10].push(id);


                for (var id_sem_0 in map.actuatorInfo[id][0][0]){
                    var t = map.actuatorInfo[id][0][0][id_sem_0];
                    map.trafficLights[t].changeColor(color0);
                }
                for (var id_sem_1 in map.actuatorInfo[id][0][1]){
                    var t = map.actuatorInfo[id][0][1][id_sem_1];
                    map.trafficLights[t].changeColor(color1);
                }
            }
            map.actuatorTime[map.timeCtrl] = [];
            map.timeCtrl = (map.timeCtrl+1)%10;
        });
    }
}
