//= require StreetSection
//= require StreetMap

fixture.preload("index.html.erb");

describe("StreetSection object", function() {
    
    beforeEach(function() {
        this.fixtures = fixture.load("index.html.erb", true);
    });

    var mapElement  = document.createElement('div');
    var input = document.createElement('input');
    input.setAttribute('id', 'slider');
    var mapLocation = {lat: -23.558147, lng: -46.660198};
    var mapZoom = 15;
    var map = new StreetMap(mapLocation, mapElement, mapZoom);
    var color1 = "#FFFFFF";
    var color2 = "#000000";
    var path = [{lat:-10, lng: -10}, {lat:-10, lng: -11}];
    var section = new StreetSection(path, color1, color2, map, 1);
    section.slider = $('#slider').slider();

    it("should be a Polyline on the map", function() {
        expect(section.section.constructor).toBe(google.maps.Polyline);
    });

    it("should belong to a map", function() {
        expect(section.map).toBe(map);
    });

    it("should have a color for speed of vehicles", function() {
        expect(color1).toBeDefined();
        expect(section.speedColor).toBe(color1);
    });

    it("should have a color for number of vehicles", function() {
        expect(color2).toBeDefined();
        expect(section.volumeColor).toBe(color2);
    });

    it("colors are strings beginning with a '#' character", function() {
        expect(color1).toMatch(/^#/);
        expect(color2).toMatch(/^#/);
    });

    it("colors must have 7 digits", function() {
        expect(color1.length).toEqual(7);
        expect(color2.length).toEqual(7);
    });

    it("has no data about traffic speed on creation", function() {
        expect(section.trafficSpeed).toEqual(0);
    });

    it("has no data about number of vehicles on creation", function() {
        expect(section.vehicles).toEqual(0);
    });

    it("should be able to change the 'speed color'", function() {
        section.setSpeedColor(color2);
        expect(section.speedColor).toBe(color2);
        expect(section.section.strokeColor).toEqual(color2);
    });

    it("should be able to change the 'volume color'", function() {
        section.changeToVolumeMode();
        section.setVolumeColor(color1);
        expect(section.volumeColor).toBe(color1);
        expect(section.section.strokeColor).toEqual(color1);
    });

    it("should be able to change to 'speed mode'", function() {
        section.setVolumeColor(color2);
        section.changeToSpeedMode();
        expect(section.section.strokeColor).toEqual(section.speedColor);
    });

    it("should be able to change to 'volume mode'", function() {
        section.setSpeedColor(color1);
        section.changeToVolumeMode();
        expect(section.section.strokeColor).toEqual(section.volumeColor);
    });

    it("should be able to alter the section width", function() {
        section.changeWidth(10);
        expect(section.section.strokeWeight).toEqual(10);
    });

    it("should be turned visible, if I want it to", function() {
        section.setVisibility(true);
        expect(section.section.visible).toBeTruthy();
    });

    it("should disappear from the map, if I want it to", function() {
        section.setVisibility(false);
        expect(section.section.visible).toBeFalsy();
    });

    it("should be clickable and trigger a pop-up window", function() {
        section.setInfoWindow();
        expect(section.infoWindow).toBeDefined();
    });
    
    it("should close all the infoWindows from street sections", function(){
        map.closeInfoWindows();
        for (var i in map.streetSections) {
            expect(map.streetSections[i].infoWindow.map).toBe(null);
        }
    });


    it("should return color '#008000' if speed is less than 10", function() {
        var color = section.hashSpeedColor(5);
        expect(color).toEqual('#008000');
    });

    it("should return color '#abc837' if 10 <= speed < 20", function() {
        var color = section.hashSpeedColor(12);
        expect(color).toEqual('#abc837');
    });

    it("should return color '#d4aa00' if 20 <= speed < 30", function() {
        var color = section.hashSpeedColor(21);
        expect(color).toEqual('#d4aa00');
    });

     it("should return color '#ff6600' if 30 <= speed < 40", function() {
        var color = section.hashSpeedColor(30);
        expect(color).toEqual('#ff6600');
    });

    it("should return color '#d40000' if 40 <= speed < 50", function() {
        var color = section.hashSpeedColor(45);
        expect(color).toEqual('#d40000');
    });
    
    it("should return color '#2b1100' if 50 <= speed", function() {
        var color = section.hashSpeedColor(65);
        expect(color).toEqual('#2b1100');
    });


    it("should return color '#ffd5d5' if volume is less than 1.66", function() {
        var color = section.hashVolumeColor(1);
        expect(color).toEqual('#ffd5d5');
    });
    it("should return color '#de87aa' if 1.66 <= volume 3.33", function() {
        var color = section.hashVolumeColor(2);
        expect(color).toEqual('#de87aa');
    });
    it("should return color '#ff2a7f' if 3.33 <= volume <= 5.0", function() {
        var color = section.hashVolumeColor(4);
        expect(color).toEqual('#ff2a7f');
    });
    it("should return color '#aa0088' if 5.0 <= volume <= 6.66", function() {
        var color = section.hashVolumeColor(5.1);
        expect(color).toEqual('#aa0088');
    });
    it("should return color '#441650' if 6.66 <= volume <= 8.33", function() {
        var color = section.hashVolumeColor(8);
        expect(color).toEqual('#441650');
    });
    it("should return color '#241c1f' if 8.33 <= volume", function() {
        var color = section.hashVolumeColor(9);
        expect(color).toEqual('#241c1f');
    });

    it("should update colors", function() {
        section.updateColor(section);
    })

    it("should close its infoWindow", function() {
        var value = section.closeInfoWindow();
        expect(value).toBeTruthy();
    });
    
    it("should calculate the average traffic value", function() {
        var array = [5,10,10];
        var size = 3;
        var value = section.updateTimeFrame(array, size, 10);
        expect(value).toEqual(10);
    });
    
    it("should return the first pair of speed/flux when setting color", function() {
        var value = section.setColors(1, [10,20,30,40], [1,2,3,4]);
        expect(value.speed).toEqual(10);
        expect(value.flux).toEqual(1);
    });
    
    it("should return the second pair of speed/flux when setting color", function() {
        var value = section.setColors(2, [10,20,30,40], [1,2,3,4]);
        expect(value.speed).toEqual(20);
        expect(value.flux).toEqual(2);
    });
    it("should return the third pair of speed/flux when setting color", function() {
        var value = section.setColors(3, [10,20,30,40], [1,2,3,4]);
        expect(value.speed).toEqual(30);
        expect(value.flux).toEqual(3);
    });
    it("should return the fourth pair of speed/flux when setting color", function() {
        var value = section.setColors(4, [10,20,30,40], [1,2,3,4]);
        expect(value.speed).toEqual(40);
        expect(value.flux).toEqual(4);
    });
    
    it("should be able to update color of a section", function() {
        var value = section.updateColor(section);
        expect(value).toBeTruthy();
    });

});