class CreateStreetSections < ActiveRecord::Migration
  def change
    create_table :street_sections do |t|
      t.float :latitude_start
      t.float :latitude_end
      t.float :longitude_start
      t.float :longitude_end
      t.string :cam_resource

      t.timestamps null: false
    end
  end
end
