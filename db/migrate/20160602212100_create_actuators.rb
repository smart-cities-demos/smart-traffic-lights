class CreateActuators < ActiveRecord::Migration
  def change
    create_table :actuators do |t|
      t.float :latitude
      t.float :longitude
      t.string :resource

      t.timestamps null: false
    end
  end
end
