class CreateTrafficLights < ActiveRecord::Migration
  def change
    create_table :traffic_lights do |t|
      t.integer :actuator_id
      t.float :latitude
      t.float :longitude
      t.string :street
      t.string :way
      t.integer :img_rotation
      t.boolean :init_state
      t.string :init_img
      t.integer :sensor_id

      t.timestamps null: false
    end
  end
end
