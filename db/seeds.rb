# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([[ name: 'Chicago' , [ name: 'Copenhagen'])
#   Mayor.create(name: 'Emanuel', city: cities.first)

street_sections_list = [
	#paulista block1
	[-23.556157, -46.662608, -23.556590, -46.662111],
	[-23.556443, -46.661938, -23.555706, -46.662745],
	#bela cintra
	[-23.557432, -46.662944, -23.556636, -46.662109],
	[-23.556443, -46.661896, -23.555463, -46.660810],
	#paulista block2
	[-23.556637, -46.662058, -23.557386, -46.661190],
	[-23.557253, -46.661053, -23.556477, -46.661893],
	#haddock lobo
	[-23.556229, -46.659871, -23.557291, -46.661017],
	[-23.557413, -46.661158, -23.558235, -46.662029],
	#paulista block 3
	[-23.557400, -46.661111, -23.558153, -46.660291],
	[-23.558064, -46.660171, -23.557338, -46.661032],
	
	#augusta (only one direction. FIXME)
	[-23.558994, -46.661162, -23.558197, -46.660292],
	[-23.558063, -46.660125, -23.556974, -46.659003],
	#paulista block 4
	[-23.558213, -46.660230, -23.560085, -46.658179],
	[-23.560001, -46.658077, -23.558106, -46.660125],
	#al min rocha azevedo
	[-23.560942, -46.658978, -23.560133, -46.658175],
	[-23.560000, -46.658033, -23.558378, -46.656323],
	#paulista block 5
	[-23.560134, -46.658130, -23.560991, -46.657205],
	[-23.560889, -46.657094, -23.560042, -46.658029],
	#peixoto gomide
	[-23.560233, -46.656418, -23.560887, -46.657055],
	[-23.561032, -46.657202, -23.561792, -46.657987],
	#paulista block 6
	[-23.561046, -46.657139, -23.562072, -46.655923],
	[-23.561926, -46.655816, -23.560928, -46.657053],
	#al casa branca
	[-23.562898, -46.656730, -23.562115, -46.655911],
	[-23.561932, -46.655772, -23.561626, -46.655437],
	#paulista block 7
	[-23.562120, -46.655856, -23.563732, -46.653834],
	[-23.563632, -46.653738, -23.561969, -46.655765],
	#pamplona
	[-23.562769, -46.652894, -23.563633, -46.653695],
	[-23.563773, -46.653828, -23.564656, -46.654657],
	#paulista block 8
	[-23.563773, -46.653784, -23.564965, -46.652362],
	[-23.564870, -46.652256, -23.563670, -46.653694],
	#al campinas
	[-23.565870, -46.653183, -23.565005, -46.652357],
	[-23.564871, -46.652215, -23.563968, -46.651346],
	#paulista block 9
	[-23.565005, -46.652317, -23.566210, -46.650942],
	[-23.566093, -46.650838, -23.564910, -46.652214],
	#al joaquim eugenio de lima
	[-23.565192, -46.649887, -23.566091, -46.650792],
	[-23.566252, -46.650938, -23.567078, -46.651781],
	#paulista block 10
	[-23.566250, -46.650895, -23.567705, -46.649081],
	[-23.567585, -46.649005, -23.566132, -46.650790],
	
	#brigadeiro luis antonio (only one direction. FIXME)
	[-23.568604, -46.649844, -23.567750, -46.649077],
	[-23.567580, -46.648972, -23.566495, -46.648329],
	#paulista block 11
	[-23.567753, -46.649038, -23.569023, -46.647496],
	[-23.568848, -46.647350, -23.567620, -46.648960],
	#carlos sampaio
	[-23.567989, -46.646536, -23.568844, -46.647304],
	[-23.569063, -46.647489, -23.569908, -46.648328],
	#paulista block 12
	[-23.569061, -46.647447, -23.569633, -46.646637],
	[-23.569500, -46.646495, -23.568883, -46.647305],
	#teixeira da silva
	[-23.570540, -46.647437, -23.569671, -46.646633],
	[-23.569499, -46.646452, -23.568710, -46.645678],
	#paulista block 13
	[-23.569678, -46.646581, -23.571334, -46.644348],
	[-23.571238, -46.644251, -23.569535, -46.646447],
	#treze de maio
	[-23.571022, -46.643934, -23.571236, -46.644203],
	[-23.571374, -46.644336, -23.571756, -46.644470]
]

actuators_list = [
	[-23.571307, -46.644273], #treze de maio (suburb)
	[-23.569585, -46.646545], #teixeira da silva (downtown)
	[-23.568954, -46.647395], #maria figueiredo / carlos sampaio (suburb)
	[-23.567664, -46.649021], #brigadeiro (two-way)
	[-23.566175, -46.650868], #joaquim eugenio (suburb)
	[-23.564937, -46.652284], #campinas (downtown)
	[-23.563704, -46.653758], #pamplona (suburb)
	[-23.562021, -46.655841], #casa branca (downtown)
	[-23.560961, -46.657127], #peixoto gomide (suburb)
	[-23.560070, -46.658103], #min. rocha azevedo (downtown)
	[-23.558133, -46.660210], #augusta (two-way)
	[-23.557353, -46.661088], #haddock lobo (suburb)
	[-23.556542, -46.662004]  #bela cintra (downtown)
];

sensors_list = [
	[-23.55659, -46.662111],   #paulista
	[-23.555706, -46.662745],  #paulista
	[-23.556636, -46.662109],  #Bela C       
	[-23.555463, -46.66081],   #Bela C  

	[-23.557386, -46.66119],   #Paulista    
	[-23.556477, -46.661893],  #paulista
	[-23.557291, -46.661017],  #haddock
	[-23.558235, -46.662029],  #haddock

	[-23.558153, -46.660291],  #paulista
	[-23.557338, -46.661032],  #paulista
	[-23.558197, -46.660292],  #paulista
	[-23.556974, -46.659003],  #luis coelho

	[-23.560085, -46.658179],  #paulista
	[-23.558106, -46.660125],  #paulista
	[-23.560133, -46.658175],  #alameda min
	[-23.558378, -46.656323],  #alameda min

	[-23.560991, -46.657205],  #paulista
	[-23.560042, -46.658029],  #paulista
	[-23.560887, -46.657055],  #peixoto g   
	[-23.561792, -46.657987],  #peixoto g

	[-23.562072, -46.655923],  #paulista
	[-23.560928, -46.657053],  #paulista
	[-23.562115, -46.655911],  #casa branca  
	[-23.561626, -46.655437],  #casa branca

	[-23.563732, -46.653834],  #paulista
	[-23.561969, -46.655765],  #paulista
	[-23.563633, -46.653695],  #pamplona
	[-23.564656, -46.654657],  #pamplona

	[-23.564965, -46.652362],  #paulista
	[-23.56367, -46.653694],   #paulista
	[-23.565005, -46.652357],  #alameda campinas
	[-23.563968, -46.651346],  #alameda campinas

	[-23.56621, -46.650942],   #paulista   
	[-23.56491, -46.652214],   #paulista
	[-23.566091, -46.650792],  #joaquimEugenio
	[-23.567078, -46.651781],  #joaquimEugenio
	
	[-23.567705, -46.649081],  #paulista
	[-23.566132, -46.65079],   #paulista
	[-23.56775, -46.649077],   #brigadeiro
	[-23.566495, -46.648329],  #brigadeiro
	
	[-23.569023, -46.647496],  #paulista
	[-23.56762, -46.64896],    #paulista
	[-23.568844, -46.647304],  #carlosSampaio
	[-23.569908, -46.648328],  #mariaFigueiredo
	
	[-23.569633, -46.646637],  #paulista
	[-23.568883, -46.647305],  #paulista
	[-23.569671, -46.646633],  #teixeiraSilva
	[-23.56871, -46.645678],   #teixeiraSilva

	[-23.571334, -46.644348],  #paulista
	[-23.569535, -46.646447],  #paulista
	[-23.571236, -46.644203],  #oswaldoCruz
	[-23.571756, -46.64447]    #oswaldoCruz
]


traffic_lights_list = [
	[-23.556378, -46.662010], #paulista w/ bela cintra (consolação)        sensor: 6
	[-23.556692, -46.662028], #paulista w/ bela cintra (paraíso)           sensor: 1
	[-23.556358, -46.661807], #paulista w/ bela cintra (downtown)          sensor: 3
	[-23.557157, -46.661116], #paulista w/ haddock lobo (consolação)       sensor: 10
	[-23.557493, -46.661046], #paulista w/ haddock lobo (paraíso)          sensor: 5
	[-23.557532, -46.661294], #paulista w/ haddock lobo (suburb)           sensor: 7
	[-23.558000, -46.660240], #paulista w/ augusta (consolação)            sensor: 14
	[-23.558225, -46.660198], #paulista w/ augusta (paraíso)               sensor: 9
	[-23.558036, -46.660074], #paulista w/ augusta (downtown)              sensor: 11
	[-23.558264, -46.660417], #paulista w/ augusta (suburb)                sensor: 12
	[-23.559972, -46.658111], #paulista w/ min. rocha azevedo (consolação) sensor: 18
	[-23.560147, -46.658117], #paulista w/ min. rocha azevedo (paraíso)    sensor: 13
	[-23.559975, -46.658006], #paulista w/ min. rocha azevedo (downtown)   sensor: 15
	[-23.560859, -46.657130], #paulista w/ peixoto gomide (consolação)     sensor: 22
	[-23.561054, -46.657133], #paulista w/ peixoto gomide (paraíso)        sensor: 17
	[-23.561063, -46.657246], #paulista w/ peixoto gomide (suburb)         sensor: 19
	[-23.561905, -46.655847], #paulista w/ casa branca (consolação)        sensor: 26
	[-23.562123, -46.655858], #paulista w/ casa branca (paraíso)           sensor: 21
	[-23.561897, -46.655744], #paulista w/ casa branca (downtown)          sensor: 23
	[-23.563604, -46.653778], #paulista w/ pamplona (consolação)           sensor: 30 
	[-23.563795, -46.653761], #paulista w/ pamplona (paraíso)              sensor: 25
	[-23.563797, -46.653859], #paulista w/ pamplona (suburb)               sensor: 27
	[-23.564846, -46.652285], #paulista w/ campinas (consolação)           sensor: 34
	[-23.565026, -46.652294], #paulista w/ campinas (paraíso)              sensor: 29
	[-23.564843, -46.652194], #paulista w/ campinas (downtown)             sensor: 31
	[-23.566064, -46.650875], #paulista w/ joaquim eugenio (consolação)    sensor: 38
	[-23.566267, -46.650877], #paulista w/ joaquim eugenio (paraíso)       sensor: 33
	[-23.566275, -46.650965], #paulista w/ joaquim eugenio (suburb)        sensor: 35
	[-23.567803, -46.648983], #paulista w/ brigadeiro (consolação)         sensor: 42 
	[-23.567540, -46.649062], #paulista w/ brigadeiro (paraíso)            sensor: 37
	[-23.567526, -46.648916], #paulista w/ brigadeiro (downtown)           sensor: 39
	[-23.567817, -46.649155], #paulista w/ brigadeiro (suburb)             sensor: 40
	[-23.568841, -46.647356], #paulista w/ carlos sampaio (consolação)     sensor: 46
	[-23.569063, -46.647446], #paulista w/ carlos sampaio (paraíso)        sensor: 41
	[-23.569067, -46.647491], #paulista w/ carlos sampaio (suburb)         sensor: 43
	[-23.569492, -46.646506], #paulista w/ teixeira da silva (consolação)  sensor: 50
	[-23.569671, -46.646589], #paulista w/ teixeira da silva (paraíso)     sensor: 45
	[-23.569491, -46.646446], #paulista w/ teixeira da silva (downtown)    sensor: 47
	[-23.571210, -46.644281], #paulista w/ treze de maio (consolação)      sensor: 52
	[-23.571392, -46.644269], #paulista w/ treze de maio (paraíso)         sensor: 49
	[-23.571397, -46.644356]  #paulista w/ treze de maio (suburb)          sensor: 51
]


#creating street_sections
i = 0
sensors = []
street_sections_list.each do |lat_s, lng_s, lat_e, lng_e|
	section = StreetSection.create(latitude_start:lat_s, latitude_end:lat_e, longitude_start:lng_s, longitude_end:lng_e)

	#creating sensors
	latlng = sensors_list[i]
	sensor = section.create_sensor(latitude:latlng[0], longitude:latlng[1], speed:0, density:0)
	sensors << sensor
	i += 1
end

#creating actuators
actuators = []
actuators_list.each do |lat, lng|
	actuator = Actuator.create(latitude:lat, longitude: lng)
	actuators << actuator
end

#creating traffic_lights
#paulista w/ bela cintra
TrafficLight.create(sensor_id:6, actuator_id:actuators[0].id, latitude:traffic_lights_list[0][0], longitude: traffic_lights_list[0][1], street:"Bela Cintra", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:1, actuator_id:actuators[0].id, latitude:traffic_lights_list[1][0], longitude: traffic_lights_list[1][1], street:"Bela Cintra", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:3, actuator_id:actuators[0].id, latitude:traffic_lights_list[2][0], longitude: traffic_lights_list[2][1], street:"Bela Cintra", way:"Downtown", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ haddock lobo
TrafficLight.create(sensor_id:10, actuator_id:actuators[1].id, latitude:traffic_lights_list[3][0], longitude: traffic_lights_list[3][1], street:"Haddock Lobo", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:5, actuator_id:actuators[1].id, latitude:traffic_lights_list[4][0], longitude: traffic_lights_list[4][1], street:"Haddock Lobo", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:7, actuator_id:actuators[1].id, latitude:traffic_lights_list[5][0], longitude: traffic_lights_list[5][1], street:"Haddock Lobo", way:"Suburb", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ augusta
TrafficLight.create(sensor_id:14, actuator_id:actuators[2].id, latitude:traffic_lights_list[6][0], longitude: traffic_lights_list[6][1], street:"Augusta", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:9, actuator_id:actuators[2].id, latitude:traffic_lights_list[7][0], longitude: traffic_lights_list[7][1], street:"Augusta", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:11, actuator_id:actuators[2].id, latitude:traffic_lights_list[8][0], longitude: traffic_lights_list[8][1], street:"Augusta", way:"Downtown", init_state:false, init_img:"assets/semaphore_red_small.png")
TrafficLight.create(sensor_id:12, actuator_id:actuators[2].id, latitude:traffic_lights_list[9][0], longitude: traffic_lights_list[9][1], street:"Augusta", way:"Suburb", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ min. rocha azevedo
TrafficLight.create(sensor_id:18, actuator_id:actuators[3].id, latitude:traffic_lights_list[10][0], longitude: traffic_lights_list[10][1], street:"Min. Rocha Azevedo", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:13, actuator_id:actuators[3].id, latitude:traffic_lights_list[11][0], longitude: traffic_lights_list[11][1], street:"Min. Rocha Azevedo", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:15, actuator_id:actuators[3].id, latitude:traffic_lights_list[12][0], longitude: traffic_lights_list[12][1], street:"Min. Rocha Azevedo", way:"Downtown", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ peixoto gomide
TrafficLight.create(sensor_id:22, actuator_id:actuators[4].id, latitude:traffic_lights_list[13][0], longitude: traffic_lights_list[13][1], street:"Peixoto Gomide", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:17, actuator_id:actuators[4].id, latitude:traffic_lights_list[14][0], longitude: traffic_lights_list[14][1], street:"Peixoto Gomide", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:19, actuator_id:actuators[4].id, latitude:traffic_lights_list[15][0], longitude: traffic_lights_list[15][1], street:"Peixoto Gomide", way:"Suburb", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ casa branca
TrafficLight.create(sensor_id:26, actuator_id:actuators[5].id, latitude:traffic_lights_list[16][0], longitude: traffic_lights_list[16][1], street:"Casa Branca", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:21, actuator_id:actuators[5].id, latitude:traffic_lights_list[17][0], longitude: traffic_lights_list[17][1], street:"Casa Branca", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:23, actuator_id:actuators[5].id, latitude:traffic_lights_list[18][0], longitude: traffic_lights_list[18][1], street:"Casa Branca", way:"Downtown", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ pamplona
TrafficLight.create(sensor_id:30, actuator_id:actuators[6].id, latitude:traffic_lights_list[19][0], longitude: traffic_lights_list[19][1], street:"Pamplona", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:25, actuator_id:actuators[6].id, latitude:traffic_lights_list[20][0], longitude: traffic_lights_list[20][1], street:"Pamplona", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:27, actuator_id:actuators[6].id, latitude:traffic_lights_list[21][0], longitude: traffic_lights_list[21][1], street:"Pamplona", way:"Suburb", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ campinas
TrafficLight.create(sensor_id:34, actuator_id:actuators[7].id, latitude:traffic_lights_list[22][0], longitude: traffic_lights_list[22][1], street:"Campinas", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:29, actuator_id:actuators[7].id, latitude:traffic_lights_list[23][0], longitude: traffic_lights_list[23][1], street:"Campinas", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:31, actuator_id:actuators[7].id, latitude:traffic_lights_list[24][0], longitude: traffic_lights_list[24][1], street:"Campinas", way:"Downtown", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ joaquim eugenio
TrafficLight.create(sensor_id:38, actuator_id:actuators[8].id, latitude:traffic_lights_list[25][0], longitude: traffic_lights_list[25][1], street:"Joaquim Eugenio", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:33, actuator_id:actuators[8].id, latitude:traffic_lights_list[26][0], longitude: traffic_lights_list[26][1], street:"Joaquim Eugenio", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:35, actuator_id:actuators[8].id, latitude:traffic_lights_list[27][0], longitude: traffic_lights_list[27][1], street:"Joaquim Eugenio", way:"Suburb", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ brigadeiro
TrafficLight.create(sensor_id:42, actuator_id:actuators[9].id, latitude:traffic_lights_list[28][0], longitude: traffic_lights_list[28][1], street:"Brigadeiro Luis Antonio", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:37, actuator_id:actuators[9].id, latitude:traffic_lights_list[29][0], longitude: traffic_lights_list[29][1], street:"Brigadeiro Luis Antonio", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:39, actuator_id:actuators[9].id, latitude:traffic_lights_list[30][0], longitude: traffic_lights_list[30][1], street:"Brigadeiro Luis Antonio", way:"Downtown", init_state:false, init_img:"assets/semaphore_red_small.png")
TrafficLight.create(sensor_id:40, actuator_id:actuators[9].id, latitude:traffic_lights_list[31][0], longitude: traffic_lights_list[31][1], street:"Brigadeiro Luis Antonio", way:"Suburb", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ carlos sampaio
TrafficLight.create(sensor_id:46, actuator_id:actuators[10].id, latitude:traffic_lights_list[32][0], longitude: traffic_lights_list[32][1], street:"Carlos Sampaio", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:41, actuator_id:actuators[10].id, latitude:traffic_lights_list[33][0], longitude: traffic_lights_list[33][1], street:"Carlos Sampaio", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:43, actuator_id:actuators[10].id, latitude:traffic_lights_list[34][0], longitude: traffic_lights_list[34][1], street:"Carlos Sampaio", way:"Suburb", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ teixeira da silva
TrafficLight.create(sensor_id:50, actuator_id:actuators[11].id, latitude:traffic_lights_list[35][0], longitude: traffic_lights_list[35][1], street:"Teixeira da Silva", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:45, actuator_id:actuators[11].id, latitude:traffic_lights_list[36][0], longitude: traffic_lights_list[36][1], street:"Teixeira da Silva", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:47, actuator_id:actuators[11].id, latitude:traffic_lights_list[37][0], longitude: traffic_lights_list[37][1], street:"Teixeira da Silva", way:"Downtown", init_state:false, init_img:"assets/semaphore_red_small.png")
#paulista w/ treze de maio
TrafficLight.create(sensor_id:52, actuator_id:actuators[12].id, latitude:traffic_lights_list[38][0], longitude: traffic_lights_list[38][1], street:"Treze de Maio", way:"Consolação", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:49, actuator_id:actuators[12].id, latitude:traffic_lights_list[39][0], longitude: traffic_lights_list[39][1], street:"Treze de Maio", way:"Paraíso", init_state:true, init_img:"assets/semaphore_green_small.png")
TrafficLight.create(sensor_id:51, actuator_id:actuators[12].id, latitude:traffic_lights_list[40][0], longitude: traffic_lights_list[40][1], street:"Treze de Maio", way:"Suburb", init_state:false, init_img:"assets/semaphore_red_small.png")


#creating actuator_sensors
#direct_influence = 0  -- Paulista
#direct_influence = 0  -- cross
ActuatorSensor.create(sensor_id:sensors[0].id,   actuator_id:actuators[0].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[1].id,   actuator_id:actuators[0].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[2].id,   actuator_id:actuators[0].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[3].id,   actuator_id:actuators[0].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[4].id,   actuator_id:actuators[0].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[5].id,   actuator_id:actuators[0].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[4].id,   actuator_id:actuators[1].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[5].id,   actuator_id:actuators[1].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[6].id,   actuator_id:actuators[1].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[7].id,   actuator_id:actuators[1].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[8].id,   actuator_id:actuators[1].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[9].id,   actuator_id:actuators[1].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[8].id,   actuator_id:actuators[2].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[9].id,   actuator_id:actuators[2].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[10].id,  actuator_id:actuators[2].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[11].id,  actuator_id:actuators[2].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[12].id,  actuator_id:actuators[2].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[13].id,  actuator_id:actuators[2].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[12].id,  actuator_id:actuators[3].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[13].id,  actuator_id:actuators[3].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[14].id,  actuator_id:actuators[3].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[15].id,  actuator_id:actuators[3].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[16].id,  actuator_id:actuators[3].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[17].id,  actuator_id:actuators[3].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[16].id,  actuator_id:actuators[4].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[17].id,  actuator_id:actuators[4].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[18].id,  actuator_id:actuators[4].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[19].id,  actuator_id:actuators[4].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[20].id,  actuator_id:actuators[4].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[21].id,  actuator_id:actuators[4].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[20].id,  actuator_id:actuators[5].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[21].id,  actuator_id:actuators[5].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[22].id,  actuator_id:actuators[5].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[23].id,  actuator_id:actuators[5].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[24].id,  actuator_id:actuators[5].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[25].id,  actuator_id:actuators[5].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[24].id,  actuator_id:actuators[6].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[25].id,  actuator_id:actuators[6].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[26].id,  actuator_id:actuators[6].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[27].id,  actuator_id:actuators[6].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[28].id,  actuator_id:actuators[6].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[29].id,  actuator_id:actuators[6].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[28].id,  actuator_id:actuators[7].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[29].id,  actuator_id:actuators[7].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[30].id,  actuator_id:actuators[7].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[31].id,  actuator_id:actuators[7].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[32].id,  actuator_id:actuators[7].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[33].id,  actuator_id:actuators[7].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[32].id,  actuator_id:actuators[8].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[33].id,  actuator_id:actuators[8].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[34].id,  actuator_id:actuators[8].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[35].id,  actuator_id:actuators[8].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[36].id,  actuator_id:actuators[8].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[37].id,  actuator_id:actuators[8].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[36].id,  actuator_id:actuators[9].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[37].id,  actuator_id:actuators[9].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[38].id,  actuator_id:actuators[9].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[39].id,  actuator_id:actuators[9].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[40].id,  actuator_id:actuators[9].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[41].id,  actuator_id:actuators[9].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[40].id, actuator_id:actuators[10].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[41].id, actuator_id:actuators[10].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[42].id, actuator_id:actuators[10].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[43].id, actuator_id:actuators[10].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[44].id, actuator_id:actuators[10].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[45].id, actuator_id:actuators[10].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[44].id, actuator_id:actuators[11].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[45].id, actuator_id:actuators[11].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[46].id, actuator_id:actuators[11].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[47].id, actuator_id:actuators[11].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[48].id, actuator_id:actuators[11].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[49].id, actuator_id:actuators[11].id, direct_influence:true)

ActuatorSensor.create(sensor_id:sensors[48].id, actuator_id:actuators[12].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[49].id, actuator_id:actuators[12].id, direct_influence:true)
ActuatorSensor.create(sensor_id:sensors[50].id, actuator_id:actuators[12].id, direct_influence:false)
ActuatorSensor.create(sensor_id:sensors[51].id, actuator_id:actuators[12].id, direct_influence:false)