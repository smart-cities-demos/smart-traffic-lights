require 'test_helper'

class SensorTest < ActiveSupport::TestCase
  
  test "two sensors never belong to the same street section" do
  	section1 = Sensor.find(1).street_section_id
  	section2 = Sensor.find(2).street_section_id
    assert_not_equal section1, section2
  end

  test "two sensors in the same location are duplicates" do
  	loc1 = [Sensor.find(1).latitude, Sensor.find(1).longitude]
  	loc2 = [Sensor.find(2).latitude, Sensor.find(2).longitude]
  	assert_not_equal loc1, loc2
  end

  test "latitude is a float" do
    assert_kind_of 1.0.class, Sensor.find(1).latitude
  end

  test "longitude is a float" do
    assert_kind_of 1.0.class, Sensor.find(1).longitude
  end

  test "resource is a string" do
  	assert_kind_of "address".class, Sensor.find(1).resource
  end
end
