# Smart Traffic Lights project

## Setting up repository

The easiest way is to clone the repository in your local machine:
```
git clone git@gitlab.com:smart-city-platform/smart-traffic-lights.git
cd smart-traffic-lights
```

## Dependencies
#### Rails
* `ruby 2.3`
* `rails 4.2.5.2`

#### JavaScript
* `JQuery` (for coding)
* `Bootstrap3` (for layout)
* `Teaspoon` (for testing)
	* `Jasmine` (for BDD)
	* `phantomJS` (for headless testing)
	* `Istanbul` (for code coverage)

#### PostgreSQL
1. Install version >=9.5 according to your OS (refer to: https://wiki.postgresql.org/wiki/Detailed_installation_guides)
2. Make sure the service is up and running

For additional information about dependencies, please refer to the Gemfile file in the root folder. There is also a heavy dependency on Google Maps API for this project, but this library is loaded dynamically during app exection, which means that you must have an internet connection in order to test it.

## Installing and running

Before the fun part, let's get dirty a little with PostgreSQL:
```
//creating a new user 
sudo su postgres -c psql
CREATE USER stl WITH PASSWORD 'stl';

//creating databases
CREATE DATABASE stl_development OWNER stl;
CREATE DATABASE stl_production OWNER stl;
CREATE DATABASE stl_test OWNER stl;
```

Run the following code on CLI to install all rails dependencies:
```
bundle install
```

Now it's time to migrate our model to PostgreSQL and seed the predefined data:
```
rake db:migrate
rake db:seed
```

And let's get rolling with rails server:
```
rails server
```

If everything went fine so far and the server is up on port 3000 (the default one), go to your web browser and enter the following address:

```
http://localhost:3000
```

You should see something like this:
![Smart Traffic Lights project](screenshot.png)

## Testing
During the first phase, we have concentrated efforts on developing our interface, which is basically HTML5 + CSS3 + JavaScript. Our tests were performed using the Teaspoon framework for JavaScript code, which in turn uses the Jasmine framework for BDD tests and the Istanbul NodeJS package for code coverage. Jasmine is installed automatically with the `bundle install` command, but Istanbul must be manually installed using this:
```
npm install -g istanbul
```

To run the tests, do the following in CLI:

```
bundle exec teaspoon --coverage=default
```
If everything went OK, you should see the results (both for unit tests and coverage) on terminal. However, the testing suite is configured to generate a detailed HTML file with coverage results in `smart-traffic-lights/coverage/index.html`, something like this:

![Smart Traffic Lights preliminary tests](testshot.png)

Besides, you can also run Teaspoon in the browser, if your rails server is up. To do that, just enter the following address:
```
http//localhost:3000/teaspoon
```
Now, for the Rails part during second phase, we developed tests for our model. Code coverage is given by SimpleCov gem. To run it, just enter in CLI:
```
rake test
```

## What we have so far
* Application interface layout
* Application interaction controllers
* Graphical artifacts
* Architecture (JavaScript):

![](class_diagram.png)

* The DB model scheme:

![](bd_diagram.png)




 

